package app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import data.CustomerDAO;
import data.CustomerDAOSQLImpl;
import objects.Customer;

/**
 * The CustomerDatabaseTest program implements an application
 * that tests the functionality of CustomerDAO.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/24/2018
 *
 */
class CustomerDatabaseTest {
	
	protected Customer customer = new Customer();
	protected CustomerDAO cData = null;
	Boolean testResult;

	@BeforeEach
    void init() {
    		cData = new CustomerDAOSQLImpl();
    		customer = new Customer();
    		testResult = false;
    }
	
	/**
	 * Testing if customer is properly inserted.
	 */
	@Test
	@Disabled
	void insertTest() {
		customer.setName("Ameera Huda");
		customer.setEmail("ameerahuda@baylor.edu");
		customer.setAddress("1234 Lakes Drive, Waco, TX, 77452");
		customer.setPhoneNumber("459-772-3456");
		testResult = cData.insert(customer);
		cData.setID(customer);
		assertTrue(testResult);
	}
	
	/**
	 * Testing if customer in information is properly updated.
	 */
	@Test
	@Disabled
	void updateTest() {
		customer.setName("A. Huda");
		customer.setEmail("ahuda@gmail.com");
		customer.setAddress("5678 Baylor Drive, Waco, TX, 77452");
		customer.setPhoneNumber("123-456-7890");
		customer.setCustomerID(1000L);
		testResult = cData.update(customer);
		assertTrue(testResult);
	}
	
	
	/**
	 * Testing if customer is properly deleted.
	 */
	@Test
	@Disabled
	void deleteTest() {
		testResult = cData.delete(1000L);
		assertTrue(testResult);
	}
	
	
	

}
