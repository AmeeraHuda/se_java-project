package app;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import data.CustomerDAO;
import data.CustomerDAOSQLImpl;
import data.OrderDAO;
import data.OrderDAOSQLImpl;
import objects.Customer;
import objects.Order;


/**
 * The OrderDatabaseTest program implements an application
 * that tests the use cases implemented for the project.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/24/2018
 *
 */
class OrderDatabaseTest {

	protected Order order = new Order();
	protected OrderDAO oData = null;
	protected Customer customer = new Customer();
	protected CustomerDAO cData = null;
	Boolean testResult;
	
	@BeforeEach
    void init() {
    		oData = new OrderDAOSQLImpl();
    		order = new Order();
    		cData = new CustomerDAOSQLImpl();
    		customer = new Customer();
    		
    		
    		testResult = false;
    }
	
	/**
	 * Testing Use Case: New Order Registration
	 * @result new order entered in database
	 */
	@Test
	@Disabled
	void insertTest() {
		List<Long> arrangementIDS = new ArrayList<Long>();
		arrangementIDS.add(101L);
		arrangementIDS.add(102L);
		order.setArrangementIDS(arrangementIDS);
		order.setMethodOfRecieving("Delivery");
		order.setOrderTotal(75.99);
		
		customer.setName("Amy Thomson");
		customer.setEmail("amyThomson@baylor.edu");
		customer.setAddress("7272 Parker Drive, Waco, TX, 77452");
		customer.setPhoneNumber("214-256-2323");
		
		cData.insert(customer);
		cData.setID(customer);
		
		order.setCustomerID(customer.getCustomerID());
		testResult = oData.insert(order);
		assertTrue(testResult);
	}
	
	/**
	 * Testing Use Case: Previous Order Update
	 * @result previous order information update with new information
	 */
	@Test
	@Disabled
	void updateTest() {
		List<Long> arrangementIDS = new ArrayList<Long>();
		arrangementIDS.add(102L);
		order.setArrangementIDS(arrangementIDS);
		order.setCustomerID(1300L);
		order.setMethodOfRecieving("Delivery");
		order.setOrderTotal(99.99);
		order.setOrderID(301L);
		testResult = oData.update(order);
		assertTrue(testResult);
	}
	
	/**
	 * Testing Use Case: Previous Order Deletion
	 * @result previous order deleted from database
	 */
	@Test
	@Disabled
	void deleteTest() {
		order.setOrderID(301L);
		testResult = oData.delete(order);
		assertTrue(testResult);
	}

}
