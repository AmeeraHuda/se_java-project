package connectionToDB;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This main program tests the connection to the database.
 * @author Ameera Huda
 * @version 1.0
 * @since 04/11/2018
 */
public class Connect {
	public static void main(String argv[]) {
		DatabaseConnection connect = DatabaseConnection.getInstance();

        Connection connection = null;

        try {
			connection = connect.getConnection();
			if (connection != null) {
				System.out.println("You made it, take control of your database now!");
			} else {
				System.out.println("Failed to make connection!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
