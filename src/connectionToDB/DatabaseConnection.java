package connectionToDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This program implements a method for connecting to a database 
 * using the design pattern Singleton.
 * @author Ameera Huda
 * @version 1.0
 * @since 04/11/2018
 *
 */
public class DatabaseConnection {
	
	public static final String JDBC_URL = "jdbc:derby:projectDatabase;create=true";
	
	private static DatabaseConnection INSTANCE;
	
	public static DatabaseConnection getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new DatabaseConnection();
		}
		return INSTANCE;
	}
	
	public Connection getConnection() throws SQLException {
		Connection connection = null;
		connection = DriverManager.getConnection(JDBC_URL);
		return connection;
	}
}
