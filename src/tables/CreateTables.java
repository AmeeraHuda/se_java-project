package tables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This program has methods to create the tables storing object information.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/14/2018
 *
 */
public class CreateTables {
	
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:projectDatabase;create=true";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";
	
	public static void main(String[] argv) {
		Connection dbConnection = null;
		Statement statement = null;
		
		dbConnection = getDBConnection();
		
		try {
			String deleteTableSQL = "DROP TABLE CUSTOMERS";                    
			statement = dbConnection.createStatement();          
			System.out.println(deleteTableSQL);                  
		                                                     
			// execute delete SQL statement                      
			statement.execute(deleteTableSQL);                   
			System.out.println("Customers has been deleted!");
		} catch( SQLException e) {
			//e.printStackTrace();
		}
		try {
			String deleteTableSQL = "DROP TABLE ORDERS";                    
			statement = dbConnection.createStatement();          
			System.out.println(deleteTableSQL);                  
		                                                     
			// execute delete SQL statement                      
			statement.execute(deleteTableSQL);                   
			System.out.println("Orders has been deleted!");
		} catch( SQLException e) {
			//e.printStackTrace();
		}
		try {
			createCustomerTable();
			createOrdersTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	
	private static void createCustomerTable() throws SQLException{
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		
		String createTableSQL = "CREATE TABLE CUSTOMERS(" + "NAME VARCHAR(255) NOT NULL, " 
				+ "EMAIL VARCHAR(255) NOT NULL, " + "ADDRESS VARCHAR(255) NOT NULL, " 
				+ "NUMBER VARCHAR(255) NOT NULL, " + "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1000, INCREMENT BY 1)" +")";
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(createTableSQL);
			
			// execute the SQL statement
			statement.execute(createTableSQL);
			System.out.println("Table \"Customers\" is created!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		
	}
	
	private static void createArrangementsTable() throws SQLException{
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		
		String createTableSQL = "CREATE TABLE ARRANGEMENTS(" + "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 100, INCREMENT BY 1), " 
				+ "FLOWERS VARCHAR(255) NOT NULL, " + "TIME_IN_DAYS BIGINT NOT NULL, " 
				+ "PRICE FLOAT NOT NULL " +")";
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(createTableSQL);
			
			// execute the SQL statement
			statement.execute(createTableSQL);
			System.out.println("Table \"Arrangements\" is created!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		
	}
	
	private static void createOrdersTable() throws SQLException{
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		
		String createTableSQL = "CREATE TABLE ORDERS(" + "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), " 
				+ "RETRIEVING_METHOD VARCHAR(255) NOT NULL, " + "TOTAL FLOAT NOT NULL, " + "CUSTOMER_ID BIGINT NOT NULL, " + "ARRANGEMENTS_IDS VARCHAR(255) NOT NULL " + ")";
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(createTableSQL);
			
			// execute the SQL statement
			statement.execute(createTableSQL);
			System.out.println("Table \"Orders\" is created!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		
	}
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

}
