package tables;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;


import data.FlowerArrangementDAO;
import data.FlowerArrangementDAOSQLImpl;

import objects.FlowerArrangement;


public class AddArrangements {
	public static void main(String argv[]) {
		FlowerArrangementDAO addArrangement = new FlowerArrangementDAOSQLImpl();
		File file = new File("FlowerArrangements.csv");
		
		Scanner scanFile = null;
		
		try {
			scanFile = new Scanner(new BufferedReader(new FileReader(file)));
		
			scanFile.nextLine();
			while( scanFile.hasNext() ) {
				String line = scanFile.nextLine();
				
				FlowerArrangement arr = new FlowerArrangement();
				String[] arrangementsInfoRetriever = line.split(";");
				
				arr.setTypesOfFlowers(arrangementsInfoRetriever[0]);
				arr.setAmtOfTimeToMake(Integer.parseInt(arrangementsInfoRetriever[1]));
				arr.setPrice(Double.parseDouble(arrangementsInfoRetriever[2]));
				
				addArrangement.insert(arr);
			}
			
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if(scanFile != null) {
				scanFile.close();
			}
		}
		
		/*FlowerArrangement arr = new FlowerArrangement();
		arr.setTypesOfFlowers("yellow roses, Asiatic lilies, light pink mini carnations, lavender daisy poms");
		arr.setAmtOfTimeToMake(2);
		arr.setPrice((long)49.99);
		arrAddition.insert(arr);
		
		FlowerArrangement arr1 = new FlowerArrangement();
		arr1.setTypesOfFlowers("hot pink roses and carnations; orange Asiatic lilies and Peruvian lilies (alstroemeria); yellow sunflowers");
		arr1.setAmtOfTimeToMake(3);
		arr1.setPrice((long)69.99);
		arrAddition.insert(arr1);
		
		FlowerArrangement arr2 = new FlowerArrangement();
		arr2.setTypesOfFlowers("pink, white and yellow Asiatic lilies");
		arr2.setAmtOfTimeToMake(3);
		arr2.setPrice((long)69.99);
		arrAddition.insert(arr2);
		
		
		FlowerArrangement arr3 = new FlowerArrangement();
		arr3.setTypesOfFlowers("Orange roses, peach miniature carnations, hot pink matsumoto asters, blue sinuata statice");
		arr3.setAmtOfTimeToMake(3);
		arr3.setPrice((long)69.99);
		arrAddition.insert(arr3);
		
		List<FlowerArrangement> arrs = new ArrayList<FlowerArrangement>();
		arrs = arrAddition.getAllArrangements();
		
		CustomerDAO add = new CustomerDAOSQLImpl();
		Customer customer = new Customer();
		customer.setName("Ameera Huda");
		customer.setEmail("ameerahuda@gmail.com");
		customer.setAddress("1234 Purple Square Dr");
		customer.setPhoneNumber("123-456-7890");
		add.insert(customer);*/
		
		/*FlowerArrangementDAO arrAddition = new FlowerArrangementDAOSQLImpl();
		FlowerArrangement arr = new FlowerArrangement();
		arr.setTypesOfFlowers("yellow roses, Asiatic lilies, light pink mini carnations, lavender daisy poms");
		arr.setAmtOfTimeToMake(3);
		arr.setPrice((long)149.99);
		arrAddition.update(arr);*/
		
		/*CustomerDAO add = new CustomerDAOSQLImpl();
		Customer customer = new Customer();
		customer.setName("Ameera Huda");
		customer.setEmail("ameerahuda@gmail.com");
		customer.setAddress("1234 Purple Square Dr");
		customer.setPhoneNumber("123-456-7890");
		add.insert(customer);
		add.setID(customer);
		
		OrderDAO addOrder = new OrderDAOSQLImpl();
		Order order = new Order();
		order.setMethodOfRecieving("Delivery");
		order.setOrderTotal(79L);
		order.setCustomerID(customer.getCustomerID());
		List<Long> arrangementIDS = new ArrayList<Long>();
		arrangementIDS.add(1L);
		arrangementIDS.add(101L);
		order.setArrangementIDS(arrangementIDS);
		addOrder.insert(order);*/
		
	}
}
