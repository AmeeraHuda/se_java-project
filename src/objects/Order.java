package objects;

import java.util.Date;
import java.util.List;

/**
 * This class creates a order object and implements setters/getters for the object.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/10/2018
 */
public class Order {
	
	private Long orderID;
	private String methodOfRecieving;
	private double orderTotal;
	
	private Long customerID;
	private List<Long> arrangementIDS;
	
	public Long getOrderID() {
		return orderID;
	}
	
	public void setOrderID(Long id) {
		this.orderID = id;
	}
	
	public String getMethodOfRecieving() {
		return methodOfRecieving;
	}
	
	public void setMethodOfRecieving(String method) {
		this.methodOfRecieving = method;
	}
	
	public double getOrderTotal() {
		return orderTotal;
	}
	
	public void setOrderTotal(double total) {
		this.orderTotal = total;
	}

	
	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}

	public List<Long> getArrangementIDS() {
		return arrangementIDS;
	}

	public void setArrangementIDS(List<Long> arrangementIDS) {
		this.arrangementIDS = arrangementIDS;
	}
	
	
}
