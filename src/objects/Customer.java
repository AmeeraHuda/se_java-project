package objects;

/**
 * This class creates a Customer object and implements setters/getters for the object.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/10/2018
 */
public class Customer {
	
	private String name;
	private String email;
	private String address;
	private String phoneNumber;
	private Long customerID;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String number) {
		this.phoneNumber = number;
	}
	
	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long id) {
		this.customerID = id;
	}
	
}
