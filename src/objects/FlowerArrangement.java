package objects;

import java.util.List;

/**
 * This class creates a FlowerArrangement object and implements setters/getters for the object.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/10/2018
 */
public class FlowerArrangement {
	
	private String typesOfFlowers;
	private Integer amtOfTimeToMake;
	private double price;
	private Long id;
	

	public String getTypesOfFlowers() {
		return typesOfFlowers;
	}
	
	public void setTypesOfFlowers(String types) {
		this.typesOfFlowers = types;
	}
	
	public Integer getAmtOfTimeToMake() {
		return amtOfTimeToMake;
	}
	
	public void setAmtOfTimeToMake(Integer amt) {
		this.amtOfTimeToMake = amt;
	}
	
	public Double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}
	
}
