package listenerandmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.swing.table.AbstractTableModel;

import objects.Customer;

/**
 * This program implements methods that allow for customer table information 
 * to be presented in the user interface.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class CustomerTableModel extends AbstractTableModel implements ObservantTableModel<List<Customer>> {
	
	private static final String[] columnNames = { "NAME", "EMAIL", "ADDRESS", "NUMBER", "ID" };
	
	private List<Customer> customers;
	
	public CustomerTableModel(List<Customer> selectedCustomers) {
		customers = selectedCustomers;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
		
	}
	
	private void setSelectedCustomers(List<Customer> selectedCustomers) {
		this.customers = selectedCustomers;
		this.fireTableDataChanged();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
		if(arg1 != null) {
			setSelectedCustomers(((List<Customer>)arg1));
		}
		else {
			setSelectedCustomers(new ArrayList<Customer>());
		}
		
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return customers.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		String field = columnNames[col];
		
		if( field == "NAME" ) {
			return customers.get(row).getName();
		}
		
		if( field == "EMAIL" ) {
			return customers.get(row).getEmail();
		}
		
		if( field == "ADDRESS" ) {
			return customers.get(row).getAddress();
		}
		
		if( field == "NUMBER" ) {
			return customers.get(row).getPhoneNumber();
		}
		
		if( field == "ID" ) {
			return customers.get(row).getCustomerID();
		}
		
		return null;
	}

	@Override
	public List<Customer> getObservedValue() {
		// TODO Auto-generated method stub
		return new ArrayList<Customer>(customers);
	}

}
