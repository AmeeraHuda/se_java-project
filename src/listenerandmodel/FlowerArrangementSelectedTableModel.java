package listenerandmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.AbstractTableModel;

import objects.Order;
import objects.FlowerArrangement;

/**
 * This program implements methods that allow for arrangements table information 
 * to be presented in the user interface.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class FlowerArrangementSelectedTableModel extends AbstractTableModel implements ObservantTableModel<List<FlowerArrangement>> {
	
	private static final String[] columnNames = { "ID", "FLOWERS", "TIME_IN_DAYS", "PRICE" };
	
	private List<FlowerArrangement> arrangements;
	
	public FlowerArrangementSelectedTableModel(List<FlowerArrangement> selectedArrangements) {
		arrangements = selectedArrangements;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return arrangements.size();
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
		
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		
		String field = columnNames[col];
		
		if( field == "ID" ) {
			return arrangements.get(row).getID();
		}
		
		if( field == "FLOWERS" ) {
			return arrangements.get(row).getTypesOfFlowers();
		}
		
		if( field == "TIME_IN_DAYS" ) {
			return arrangements.get(row).getAmtOfTimeToMake();
		}
		
		if( field == "PRICE" ) {
			return arrangements.get(row).getPrice();
		}
		
		return null;
	}
	
	private void setSelectedArrangements(List<FlowerArrangement> selectedArrangements) {
		this.arrangements = selectedArrangements;
		this.fireTableDataChanged();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		if(arg1 != null) {
			setSelectedArrangements(((List<FlowerArrangement>)arg1));
		}
		else {
			setSelectedArrangements(new ArrayList<FlowerArrangement>());
		}
	}

	@Override
	public List<FlowerArrangement> getObservedValue() {
		// TODO Auto-generated method stub
		return new ArrayList<FlowerArrangement>(arrangements);
	}


}
