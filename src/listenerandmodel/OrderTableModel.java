package listenerandmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;


import javax.swing.table.AbstractTableModel;

import objects.Order;

/**
 * This program implements methods that allow for order table information 
 * to be presented in the user interface.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class OrderTableModel extends AbstractTableModel implements ObservantTableModel<List<Order>> {

	private static final String[] columnNames = { "ID", "RETRIEVING_METHOD", "TOTAL", "CUSTOMER_ID", "ARRANGEMENTS_ID" };
	
	private List<Order> orders;
	
	public OrderTableModel(List<Order> selectedOrders) {
		orders = selectedOrders;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
		
	}
	
	private void setSelectedOrders(List<Order> selectedOrders) {
		this.orders = selectedOrders;
		this.fireTableDataChanged();
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
		if(arg1 != null) {
			setSelectedOrders(((List<Order>)arg1));
		}
		else {
			setSelectedOrders(new ArrayList<Order>());
		}
		
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return orders.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		String field = columnNames[col];
		
		if( field == "ID" ) {
			return orders.get(row).getOrderID();
		}
		
		if( field == "RETRIEVING_METHOD" ) {
			return orders.get(row).getMethodOfRecieving();
		}
		
		if( field == "TOTAL" ) {
			return orders.get(row).getOrderTotal();
		}
		
		if( field == "CUSTOMER_ID" ) {
			return orders.get(row).getCustomerID();
		}
		
		if( field == "ARRANGEMENTS_ID" ) {
			return orders.get(row).getArrangementIDS();
		}
		
		return null;
	}

	@Override
	public List<Order> getObservedValue() {
		// TODO Auto-generated method stub
		return new ArrayList<Order>(orders);
	}
	
	

}
