package listenerandmodel;

import java.util.Observer;

import javax.swing.table.TableModel;

/**
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 * @param <E> object used in TableModel
 */
public interface ObservantTableModel<E> extends Observer, TableModel {
	public E getObservedValue();
}
