package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objects.Customer;
import objects.FlowerArrangement;
import objects.Order;
import service.OrderService;
import service.OrderServiceImpl;
import service.Response;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This program generates the window that allows user to save the order information. 
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class SaveOrder extends JFrame {

	private JPanel contentPane;
	private JTextField totalTextField;
	
	private OrderService orderService = new OrderServiceImpl();
	private double total = 0.0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SaveOrder frame = new SaveOrder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}

	/**
	 * Create the frame.
	 */
	public SaveOrder(Customer customer, List<FlowerArrangement> arrangements) {
		
		final SaveOrder window = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblRecievingMethod = new JLabel("Recieving Method");
		
		JRadioButton rdbtnPickUp = new JRadioButton("Pick Up");
		
		JRadioButton rdbtnDelivery = new JRadioButton("Delivery");
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnPickUp);
		group.add(rdbtnDelivery);
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblRecievingMethod)
					.addGap(67)
					.addComponent(rdbtnPickUp, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(46)
					.addComponent(rdbtnDelivery, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(48, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnDelivery)
						.addComponent(rdbtnPickUp)
						.addComponent(lblRecievingMethod))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JLabel lblTotal = new JLabel("Total");
		panel_1.add(lblTotal);
		
		totalTextField = new JTextField();
		panel_1.add(totalTextField);
		totalTextField.setColumns(10);
		
		for( FlowerArrangement e : arrangements ) {
			total += e.getPrice();
		}
		
		totalTextField.setText(String.valueOf(Math.floor(total*100) / 100));
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JLabel endLabel = new JLabel("");
		panel_2.add(endLabel);
		
		JButton btnConfirm = new JButton("Confirm Order");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Order nOrder = new Order();
				nOrder.setMethodOfRecieving("");
				nOrder.setCustomerID(customer.getCustomerID());
				
				List<Long> arrIDS = new ArrayList<Long>();
				for(FlowerArrangement e1 : arrangements ) {
					arrIDS.add(e1.getID());
				}
				nOrder.setArrangementIDS(arrIDS);
				
				if( rdbtnPickUp.isSelected() ) {
					nOrder.setMethodOfRecieving("Pick Up");
				} else if( rdbtnDelivery.isSelected() ) {
					nOrder.setMethodOfRecieving("Delivery");
				}
				
				nOrder.setOrderTotal(total);
				Response response = orderService.newOrder(customer, arrangements, nOrder);
				
				if( response.isSuccess() ) {
					endLabel.setForeground(Color.BLACK);
					endLabel.setText(response.getMessage());
					window.dispose();
				}
				
				endLabel.setForeground(Color.RED);
				endLabel.setText(response.getMessage());
				
			}
		});
		panel_1.add(btnConfirm);
		
		
	}
}
