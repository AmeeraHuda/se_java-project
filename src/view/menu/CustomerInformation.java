package view.menu;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.CustomerDAO;
import data.CustomerDAOSQLImpl;
import listenerandmodel.CustomerTableModel;
import listenerandmodel.ObservantTableModel;
import objects.Customer;
import objects.Order;
import service.OrderService;
import service.OrderServiceImpl;
import view.UpdateOrder;

import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This program generates window that shows user all of the customers.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class CustomerInformation extends JFrame {

	private JPanel contentPane;
	private JTable customerTable;
	
	private List<Customer> selectedCustomers = null;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerInformation frame = new CustomerInformation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}

	/**
	 * Create the frame.
	 */
	public CustomerInformation() {
		
		CustomerInformation window = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblCustomerInformation = new JLabel("Customer Information");
		panel.add(lblCustomerInformation);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		
		
		JButton btnUpdate = new JButton("Update");
		
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.dispose();
			}
		});
		
		
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(btnUpdate)
							.addPreferredGap(ComponentPlacement.RELATED, 265, Short.MAX_VALUE)
							.addComponent(btnBack)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnUpdate)
						.addComponent(btnBack))
					.addGap(2))
		);
		
		CustomerDAO cDAO = new CustomerDAOSQLImpl();
		List<Customer> customers = cDAO.getCustomers();
		CustomerTableModel cModel = new CustomerTableModel(customers);
		
		customerTable = new JTable();
		customerTable.setModel(cModel);
		scrollPane.setViewportView(customerTable);
		panel_1.setLayout(gl_panel_1);
		
		customerTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JTable table = (JTable) e.getSource();
				ObservantTableModel<List<Customer>> cModel = (ObservantTableModel<List<Customer>>)table.getModel();
				int[] selected = table.getSelectedRows();
				final List<Customer> allSelectedCustomers = cModel.getObservedValue();
				selectedCustomers = new ArrayList<Customer>();
				for(Integer cIndex: selected) {
					selectedCustomers.add(allSelectedCustomers.get(cIndex));
				}
			}
		});
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerUpdate CUpopUp = new CustomerUpdate(selectedCustomers.get(0));
				CUpopUp.setVisible(true);
			}
		});
	}

}
