package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.menu.CustomerInformation;
import view.menu.FAInformation;
import view.menu.OrderInformation;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * This program generates the main page introduced to users when using application.
 * 
 * @author Ameera
 * @version 1.0
 *
 */
public class MainPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage frame = new MainPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null); //added
		
		//added
		JLabel profile = new JLabel();
		profile.setBounds(100, 100, 450, 300);
		String path="/Users/Ameera/Desktop/option1.jpg";
		
		// creating menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Menu");
		menuBar.add(mnNewMenu);
		
		// menu for order information
		JMenuItem ordersBtn = new JMenuItem("Order Info");
		mnNewMenu.add(ordersBtn);
		ordersBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderInformation OIpopUp = new OrderInformation();
				OIpopUp.setVisible(true);
			}
		});
		
		
		JMenuItem customerBtn = new JMenuItem("Customer Info");
		customerBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerInformation CIpopUp = new CustomerInformation();
				CIpopUp.setVisible(true);
			}
		});
		mnNewMenu.add(customerBtn);
		
		JMenuItem faBtn = new JMenuItem("Arrangement Info");
		faBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FAInformation FAIpopUp = new FAInformation();
				FAIpopUp.setVisible(true);
			}
		});
		mnNewMenu.add(faBtn);
		
		// Main Page
		JLabel lblWhatWouldYou = new JLabel("What Would You Like To Do Today?");
		lblWhatWouldYou.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel = new JPanel();
		
		JButton btnNewOrder = new JButton("New Order");
		btnNewOrder.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						NewOrderFrame NOFpopUp = new NewOrderFrame();
						NOFpopUp.setVisible(true);
					}
				});
			}
			
		});
		panel.add(btnNewOrder);
		
		JButton btnUpdateOrder = new JButton("Update Order");
		btnUpdateOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderFrame OFpopUp = new OrderFrame();
				OFpopUp.setVisible(true);
			}
		});
		panel.add(btnUpdateOrder);
		
		JButton btnDeleteOrder = new JButton("Delete Order");
		btnDeleteOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderFrame OFpopUp = new OrderFrame();
				OFpopUp.setVisible(true);
			}
		});
		panel.add(btnDeleteOrder);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(lblWhatWouldYou, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 434, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 428, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblWhatWouldYou, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(11, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		
		// added
		ImageIcon myImage = new ImageIcon(path);
		Image img = myImage.getImage();
		Image newImg = img.getScaledInstance(profile.getWidth(), profile.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImg);
		profile.setIcon(image);
		
		
	}
}
