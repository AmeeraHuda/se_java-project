package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.CustomerDAO;
import data.CustomerDAOSQLImpl;
import data.FlowerArrangementDAO;
import data.FlowerArrangementDAOSQLImpl;
import listenerandmodel.FlowerArrangementSelectedTableModel;
import listenerandmodel.ObservantTableModel;
import objects.Customer;
import objects.FlowerArrangement;
import objects.Order;
import service.OrderService;
import service.OrderServiceImpl;
import service.Response;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

/**
 * This program generates the window used by user who wants to update order information.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class UpdateOrder extends JFrame {

	private JPanel contentPane;
	private JTable flowerTable;
	
	
	List<FlowerArrangement> newArrangements = null;
	private OrderService orderService = new OrderServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateOrder frame = new UpdateOrder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}

	/**
	 * Create the frame.
	 */
	public UpdateOrder(Order order) {
		
		final UpdateOrder window = this;
		
		List<Long> selectedArrangements = order.getArrangementIDS();
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblUpdateOrder = new JLabel("Update Order");
		lblUpdateOrder.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton btnUpdate = new JButton("Update");
		
		JLabel endLabel = new JLabel("");
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblUpdateOrder)
					.addGap(84)
					.addComponent(endLabel)
					.addPreferredGap(ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
					.addComponent(btnUpdate)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUpdateOrder)
						.addComponent(btnUpdate)
						.addComponent(endLabel))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.EAST);
		
		JLabel label_4 = new JLabel("Pick an Arrangement");
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(59)
							.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 47, Short.MAX_VALUE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)))
					.addGap(0))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(5)
					.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		FlowerArrangementDAO arrDAO = new FlowerArrangementDAOSQLImpl();
		List<FlowerArrangement> arrangements = arrDAO.getAllArrangements();
		FlowerArrangementSelectedTableModel arrModel = new FlowerArrangementSelectedTableModel(arrangements);
		
		flowerTable = new JTable();
		flowerTable.setModel(arrModel);
		scrollPane.setViewportView(flowerTable);
		panel_2.setLayout(gl_panel_2);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JLabel label = new JLabel("Recieving by");
		
		JRadioButton rdbtnPickUp = new JRadioButton("Pick Up");
		
		JRadioButton rdbtnDelivery = new JRadioButton("Delivery");
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnPickUp);
		group.add(rdbtnDelivery);
		
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(19)
							.addComponent(label))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(39)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(rdbtnDelivery)
								.addComponent(rdbtnPickUp))))
					.addContainerGap(70, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnPickUp)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDelivery)
					.addContainerGap(141, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		flowerTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				JTable table = (JTable) e.getSource();
				ObservantTableModel<List<FlowerArrangement>> faModel = (ObservantTableModel<List<FlowerArrangement>>)table.getModel();
				int[] selected = table.getSelectedRows();
				final List<FlowerArrangement> allSelectedArrangements = faModel.getObservedValue();
				newArrangements = new ArrayList<FlowerArrangement>();
				for(Integer faIndex: selected) {
					newArrangements.add(allSelectedArrangements.get(faIndex));
				}
				
			}
		});
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String method = "";
				Boolean changed = false, changed1 = false;
				if( rdbtnPickUp.isSelected() ) {
					method = "Pick Up";
				} else if( rdbtnDelivery.isSelected() ) {
					method = "Delivery";
				}
				
				if(order.getMethodOfRecieving() != method && !method.equals("") ) {
					order.setMethodOfRecieving(method);
					changed = true;
				}
				
				String str  = new String();
				str = "";
			    Iterator<Long> it = order.getArrangementIDS().iterator();
			    while(it.hasNext()) {
			    		str += it.next().toString();
			    		if(it.hasNext()) {
			    			str += ",";
			    		}
			    }
			    
			    if( newArrangements != null) {
			    		String str1  = new String();
			    		str = "";
			    		Iterator<FlowerArrangement> it1 = newArrangements.iterator();
			    		while(it1.hasNext()) {
			    			str += it1.next().getID().toString();
			    			if(it1.hasNext()) {
			    				str += ",";
			    			}
			    		}
			    
			    
			    		if(!str.equals(str1)) {
			    			List<Long> newarrIDS = new ArrayList<Long>();
			    			for( FlowerArrangement e1 : newArrangements ) {
			    				newarrIDS.add(e1.getID());
			    			}
			    			order.setArrangementIDS(newarrIDS);
			    			changed1 = true;
			    		}
			    }
				
				Response response = null;
				
				if( changed1 == true ) {
					response = orderService.updateOrder(newArrangements, order);
				} else {
					response = orderService.updateOrder(null, order);
				}
				
				if( response.isSuccess() ) {
					endLabel.setForeground(Color.BLACK);
					endLabel.setText(response.getMessage());
					window.dispose();
				}
				
				endLabel.setForeground(Color.RED);
				endLabel.setText(response.getMessage());
				
			}
		});
	}
}
