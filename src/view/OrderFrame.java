package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.FlowerArrangementDAO;
import data.FlowerArrangementDAOSQLImpl;
import data.OrderDAO;
import data.OrderDAOSQLImpl;
import listenerandmodel.FlowerArrangementSelectedTableModel;
import listenerandmodel.ObservantTableModel;
import listenerandmodel.OrderTableModel;
import objects.FlowerArrangement;
import objects.Order;
import service.OrderService;
import service.OrderServiceImpl;
import service.Response;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.Timer;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This program generates the window that shows user all of the orders 
 * and option of what they may want to do with the orders.
 * 
 * @author Ameera Huda
 * @version 1.0
 *
 */
public class OrderFrame extends JFrame {

	private JPanel contentPane;
	private JTable orderTable;
	
	private List<Order> selectedOrders = null;
	private OrderService orderService = new OrderServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderFrame frame = new OrderFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}

	/**
	 * Create the frame.
	 */
	public OrderFrame() {
		
		final OrderFrame window = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel labelQuestionAndResponse = new JLabel("Which Order Would You Like To Update or Delete?");
		panel.add(labelQuestionAndResponse);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnUpdate = new JButton("Update");
		
		
		JButton btnDelete = new JButton("Delete");
		
		
		JButton btnCancel = new JButton("Back");
		
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(22)
					.addComponent(btnUpdate)
					.addGap(63)
					.addComponent(btnDelete)
					.addPreferredGap(ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
					.addComponent(btnCancel)
					.addGap(21))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdate)
						.addComponent(btnDelete)
						.addComponent(btnCancel))
					.addGap(8))
		);
		
		OrderDAO oDAO = new OrderDAOSQLImpl();
		List<Order> orders = oDAO.getOrders();
		OrderTableModel oModel = new OrderTableModel(orders);
		
		orderTable = new JTable();
		orderTable.setModel(oModel);
		orderTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JTable table = (JTable) e.getSource();
				ObservantTableModel<List<Order>> oModel = (ObservantTableModel<List<Order>>)table.getModel();
				int[] selected = table.getSelectedRows();
				final List<Order> allSelectedOrders = oModel.getObservedValue();
				selectedOrders = new ArrayList<Order>();
				for(Integer oIndex: selected) {
					selectedOrders.add(allSelectedOrders.get(oIndex));
				}
			}
		});
		scrollPane.setViewportView(orderTable);
		panel_1.setLayout(gl_panel_1);
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println(selectedOrders.get(0).getOrderID());
				UpdateOrder UOFpopUp = new UpdateOrder(selectedOrders.get(0));
				UOFpopUp.setVisible(true);
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println(selectedOrders.get(0).getOrderID());
				Response response = orderService.deleteOrder(selectedOrders.get(0));
				if( response.isSuccess() ) {
					labelQuestionAndResponse.setForeground(Color.BLACK);
					labelQuestionAndResponse.setText(response.getMessage());
				
					window.dispose();
			
				}
				
				labelQuestionAndResponse.setForeground(Color.RED);
				labelQuestionAndResponse.setText(response.getMessage());
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.dispose();
			}
		});
	}
}
