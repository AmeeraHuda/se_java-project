package data;

import java.util.List;

import objects.Customer;

/**
 * This program has the functionalities needed to communicate 
 * with the Customer Table.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/11/2018
 *
 */
public interface CustomerDAO {
	
	/**
	 * Inserts the given customer to the table holding customer data.
	 * 
	 * @param customer object that holds information of the Customer type
	 * @return returns whether or not customer insertion was successful
	 */
	public Boolean insert(Customer customer);
	
	/**
	 * Updates information in the table of the given customer.
	 * 
	 * @param customer object that holds information of the Customer type
	 * @return returns whether or not customer update was successful
	 */
	public Boolean update(Customer customer);
	
	/**
	 * Deletes the given customer from the table.
	 * 
	 * @param customerID number a specific customer is identified by
	 * @return returns whether or not customer deletion was successful
	 */
	public Boolean delete(Long customerID);
	
	/**
	 * Finds the Customer with the given id.
	 * 
	 * @param id number a specific customer is identified by
	 * @return customer information stored in the form of a Customer object
	 */
	public Customer findById(Long id);
	
	/**
	 * Stores the customerID generated by the database.
	 * 
	 * @param customer whose id needs to be set
	 */
	public void setID(Customer customer);
	
	/**
	 * Provides a collection of all the customers and their information.
	 * @return list of all the customers
	 */
	public List<Customer> getCustomers();
}
