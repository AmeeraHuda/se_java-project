package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import objects.Customer;
import objects.Order;

/**
 * This program implements the OrderDAO with no extra functionalities.
 * 
 * @author Ameera Huda
 * @version 1,0
 * @since 04/14/2018
 *
 */
public class OrderDAOSQLImpl implements OrderDAO {
	
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:projectDatabase;";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";

	@Override
	public Boolean insert(Order order) {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		
		try {
			
			String insertTableSQL = "INSERT INTO ORDERS" + "(RETRIEVING_METHOD, TOTAL, CUSTOMER_ID, ARRANGEMENTS_IDS) " + "VALUES"
						+ "(?,?,?,?)";
				
			dbConnection = getDBConnection();
			statement = dbConnection.prepareStatement(insertTableSQL);
			statement.setString(1, order.getMethodOfRecieving());
			statement.setDouble(2, order.getOrderTotal());
			statement.setLong(3, order.getCustomerID());
			
			
			String str  = new String();
			str = "";
		    Iterator<Long> it = order.getArrangementIDS().iterator();
		    while(it.hasNext()) {
		    		str += it.next().toString();
		    		if(it.hasNext()) {
		    			str += ",";
		    		}
		    }
		    str.toString();
		    statement.setString(4, str);
		   
			
			// execute insert SQL statement
			statement.executeUpdate();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public Boolean update(Order order) {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		
		try {
			String updateTableSQL = "UPDATE ORDERS SET RETRIEVING_METHOD = ?, TOTAL = ?, CUSTOMER_ID = ?, ARRANGEMENTS_IDS = ? WHERE ID = ?";
			
			dbConnection = getDBConnection();
			
			statement = dbConnection.prepareStatement(updateTableSQL);
			
			statement.setString(1, order.getMethodOfRecieving());
			statement.setDouble(2, order.getOrderTotal());
			statement.setLong(3, order.getCustomerID());
			
			String str  = new String();
			str = "";
		    Iterator<Long> it = order.getArrangementIDS().iterator();
		    while(it.hasNext()) {
		    		str += it.next().toString();
		    		if(it.hasNext()) {
		    			str += ",";
		    		}
		    }
		    str.toString();
		    statement.setString(4, str);
		    
		    statement.setLong(5, order.getOrderID());
			
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public Boolean delete(Order order) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		
		
		try {
			String deleteTableSQL = "DELETE FROM ORDERS WHERE ID = ?";

			dbConnection = getDBConnection();
			
			statement = dbConnection.prepareStatement(deleteTableSQL);
			
			statement.setLong(1, order.getOrderID());
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Could not remove the order");
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public Order findById(Long id) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		Order order = new Order();
		
		String selectTableSQL =  "SELECT * FROM ORDERS WHERE ID = " + id;
		
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false) {
				System.out.println("ResultSet in empty in Java");
			} else {
				order.setOrderID(rs.getLong(1));
				order.setMethodOfRecieving(rs.getString(2));
				order.setOrderTotal(rs.getLong(3));
				order.setCustomerID(rs.getLong(4));
				
				List<Long> list = new ArrayList<Long>();
				for (String s : rs.getString(5).split(",")) {
				    list.add(Long.parseLong(s));
				}
				order.setArrangementIDS(list);

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return order;
	}
	
	@Override
	public int countOfOrders() {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		Statement statement = null;
		int count = 0;
		
		String selectTableSQL =  "SELECT * FROM ORDERS";
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			//System.out.println(selectTableSQL);
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while( rs.next() ) {
				count++;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return count;
	}
	
	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		Statement statement = null;
		List<Order> orders = new ArrayList<Order>();
		
		String selectTableSQL =  "SELECT * FROM ORDERS";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while(rs.next()) {
				Order order = new Order();
				order.setOrderID(rs.getLong(1));
				order.setMethodOfRecieving(rs.getString(2));
				order.setOrderTotal(rs.getLong(3));
				order.setCustomerID(rs.getLong(4));
				
				List<Long> list = new ArrayList<Long>();
				for (String s : rs.getString(5).split(",")) {
				    list.add(Long.parseLong(s));
				}
				order.setArrangementIDS(list);
				orders.add(order);
			}
		} catch (SQLException e) {
			// Should replace with log message
			System.out.println("Data could not be retrieved");
			e.printStackTrace();
		}
		return orders;
	}
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}


}
