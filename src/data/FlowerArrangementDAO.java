package data;

import java.util.List;

import objects.FlowerArrangement;

/**
 * This program has the functionalities needed to communicate
 * with the arrangements table.
 * 
 * @author Ameera
 * @version 1.0
 * @since 04/14/2018
 */
public interface FlowerArrangementDAO {
	
	/**
	 * Inserts the given flower arrangement to the table.
	 * 
	 * @param flowerarrangement object that hold the arrangement information
	 * @return whether or not insertion of the arrangement was successful
	 */
	public Boolean insert(FlowerArrangement flowerarrangement);
	
	/**
	 * Updates information in the table of the given arrangement.
	 * 
	 * @param flowerarrangement object that holds information of the FlowerArrangement type
	 * @return returns whether or not arrangement update was successful
	 */
	public Boolean update(FlowerArrangement flowerarrangement);
	
	/**
	 * Deletes the given arrangement from the table.
	 * 
	 * @param flowerArrangement arrangement to be deleted
	 * @return returns whether or not customer deletion was successful
	 */
	public Boolean delete(FlowerArrangement flowerarrangement);
	
	/**
	 * Finds the arrangement with the given id.
	 * 
	 * @param id number a specific flower arrangement is identified by
	 * @return flower arrangement information stored in the form of a FlowerArrangement object
	 */
	public FlowerArrangement findById(Long id);
	
	/**
	 * Provides a collection of all the arrangements and their information.
	 * @return list of all the flower arrangements
	 */
	public List<FlowerArrangement> getAllArrangements();
	
}
