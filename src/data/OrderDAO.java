package data;

import java.util.List;

import objects.Order;

/**
 * This program has the functionalities needed to communicate
 * with the Order table.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/14/2018
 *
 */
public interface OrderDAO {
	
	/**
	 * Inserts the given order to the table holding order data.
	 * 
	 * @param order object that holds information of the Order type
	 * @return returns whether or not order insertion was successful
	 */
	public Boolean insert(Order order);
	
	/**
	 * Updates information in the table of the given order.
	 * 
	 * @param order object that holds information of the Order type
	 * @return returns whether or not order update was successful
	 */
	public Boolean update(Order order);
	
	/**
	 * Deletes the given order from the table.
	 * 
	 * @param order to be deleted
	 * @return returns whether or not order deletion was successful
	 */
	public Boolean delete(Order order);
	
	/**
	 * Finds the order with the given id.
	 * 
	 * @param id number a specific order is identified by
	 * @return order information stored in the form of a Order object
	 */
	public Order findById(Long id);
	
	/**
	 * Produces the number of orders there are in the table.
	 *
	 * @return number of orders 
	 */
	public int countOfOrders();
	
	/**
	 * Provides a collection of all the orders and their information.
	 * @return list of all the orders
	 */
	public List<Order> getOrders();
}
