package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import objects.FlowerArrangement;
import objects.Order;


/**
 * This program implements the FlowerArrangementDAO with no extra functionalities.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/14/2018
 *
 */
public class FlowerArrangementDAOSQLImpl implements FlowerArrangementDAO {
	
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:projectDatabase;";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";

	@Override
	public Boolean insert(FlowerArrangement flowerarrangement) {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		
		try {
			
			String insertTableSQL = "INSERT INTO ARRANGEMENTS" + "(FLOWERS, TIME_IN_DAYS, PRICE) " + "VALUES"
						+ "(?,?,?)";
				
			dbConnection = getDBConnection();
			statement = dbConnection.prepareStatement(insertTableSQL);
			statement.setString(1, flowerarrangement.getTypesOfFlowers());
			statement.setInt(2, flowerarrangement.getAmtOfTimeToMake());
			statement.setDouble(3, flowerarrangement.getPrice());
			
			// execute insert SQL statement
			statement.executeUpdate();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public Boolean update(FlowerArrangement flowerarrangement) {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		
		try {
			String updateTableSQL = "UPDATE ARRANGEMENTS SET FLOWERS = ?, TIME_IN_DAYS = ?, PRICE = ? WHERE ID = ?";
			
			dbConnection = getDBConnection();
			
			statement = dbConnection.prepareStatement(updateTableSQL);
			statement.setString(1, flowerarrangement.getTypesOfFlowers());
			statement.setInt(2, flowerarrangement.getAmtOfTimeToMake());
			statement.setDouble(3, flowerarrangement.getPrice());
			statement.setLong(4, flowerarrangement.getID());
			
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	@Override
	public Boolean delete(FlowerArrangement flowerarrangement) {
		// TODO Auto-generated method stub
		/*Connection dbConnection = null;
		PreparedStatement statement = null;
		
		try {
			String deleteTableSQL = "DELETE FROM ARRANGEMENTS WHERE ID = ?";

			dbConnection = getDBConnection();
			
			statement.setLong(1, flowerarrangement.getID());
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Could not remove the arrangement");
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;*/
		return false;
	}

	@Override
	public FlowerArrangement findById(Long id) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		FlowerArrangement arrangement = new FlowerArrangement();
		
		String selectTableSQL =  "SELECT * FROM ARRANGEMENTS WHERE ID = " + id;
		
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false) {
				System.out.println("ResultSet in empty in Java");
			} else {
				
				arrangement.setID((rs.getLong(1)));
				arrangement.setTypesOfFlowers((rs.getString(2)));
				arrangement.setAmtOfTimeToMake(rs.getInt(3));
				arrangement.setPrice(rs.getDouble(4));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return arrangement;
		
	}
	
	@Override
	public List<FlowerArrangement> getAllArrangements() {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		Statement statement = null;
		List<FlowerArrangement> arrangements = new ArrayList<FlowerArrangement>();
		
		String selectTableSQL =  "SELECT * FROM ARRANGEMENTS";
		try {
			
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while(rs.next()) {
				FlowerArrangement arrangement = new FlowerArrangement();
				arrangement.setID((rs.getLong(1)));
				arrangement.setTypesOfFlowers((rs.getString(2)));
				arrangement.setAmtOfTimeToMake(rs.getInt(3));
				arrangement.setPrice(rs.getDouble(4));
				arrangements.add(arrangement);
			}
		} catch (SQLException e) {
			// Should replace with log message
			System.out.println("Data could not be retrieved");
			e.printStackTrace();
		}
		return arrangements;
	}

	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	
}
