package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import objects.Customer;

/**
 * This program implements the CustomerDAO interface with no extra functionalities.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/14/2018
 *
 */
public class CustomerDAOSQLImpl implements CustomerDAO {
	
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:projectDatabase;create=true";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";


	@Override
	public Boolean insert(Customer customer) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		try {
			
			String insertTableSQL = "INSERT INTO CUSTOMERS" + "(NAME, EMAIL, ADDRESS, NUMBER) " + "VALUES"
						+ "(?,?,?,?)";
				
			dbConnection = getDBConnection();
			statement = dbConnection.prepareStatement(insertTableSQL);
			statement.setString(1, customer.getName());
			statement.setString(2, customer.getEmail());
			statement.setString(3, customer.getAddress());
			statement.setString(4, customer.getPhoneNumber());
			
			// execute insert SQL statement
			statement.executeUpdate();
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
		
	}

	@Override
	public Boolean update(Customer customer) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		try {
			String updateTableSQL = "UPDATE CUSTOMERS SET NAME = ?, EMAIL = ?, ADDRESS = ?, NUMBER = ? WHERE ID = ?";
			
			dbConnection = getDBConnection();
			
			statement = dbConnection.prepareStatement(updateTableSQL);
			statement.setString(1, customer.getName());
			statement.setString(2, customer.getEmail());
			statement.setString(3, customer.getAddress());
			statement.setString(4, customer.getPhoneNumber());
			statement.setLong(5, customer.getCustomerID());
			
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
		
	}

	@Override
	public Boolean delete(Long customerID) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		try {
			String deleteTableSQL = "DELETE FROM CUSTOMERS WHERE ID = ?";

			dbConnection = getDBConnection();
			statement = dbConnection.prepareStatement(deleteTableSQL);
			statement.setLong(1, customerID);
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Could not remove the customer");
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
		
	}

	@Override
	public Customer findById(Long id) {
		// TODO Auto-generated method stub
		
		Connection dbConnection = null;
		Statement statement = null;
		Customer customerFound = new Customer();
		
		String selectTableSQL =  "SELECT * FROM CUSTOMERS WHERE ID = " + id;
		
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false) {
				System.out.println("ResultSet in empty in Java");
			} else {
				
				customerFound.setName(rs.getString(1));
				customerFound.setEmail(rs.getString(2));
				customerFound.setAddress(rs.getString(3));
				customerFound.setPhoneNumber(rs.getString(4));
				customerFound.setCustomerID(rs.getLong(5));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return customerFound;
		
	}
	
	public void setID(Customer customer) {
		Connection dbConnection = null;
		PreparedStatement statement = null;
		
		String selectTableSQL =  "SELECT ID FROM CUSTOMERS WHERE NAME = ?";
		
		
		try {
			dbConnection = getDBConnection();
			
			statement = dbConnection.prepareStatement(selectTableSQL);
			statement.setString(1, customer.getName());
			
			// execute select SQL statement
			ResultSet rs = statement.executeQuery();
			if (rs.next() == false) {
				System.out.println("ResultSet in empty in Java");
			} else {
				
				customer.setCustomerID(rs.getLong(1));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public List<Customer> getCustomers() {
		// TODO Auto-generated method stub
		Connection dbConnection = null;
		Statement statement = null;
		List<Customer> customers = new ArrayList<Customer>();
		
		String selectTableSQL =  "SELECT * FROM CUSTOMERS";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while(rs.next()) {
				Customer customer = new Customer();
				customer.setName(rs.getString(1));
				customer.setEmail(rs.getString(2));
				customer.setAddress(rs.getString(3));
				customer.setPhoneNumber(rs.getString(4));
				customer.setCustomerID(rs.getLong(5));
				
				customers.add(customer);
			}
		} catch (SQLException e) {
			// Should replace with log message
			System.out.println("Data could not be retrieved");
			e.printStackTrace();
		}
		return customers;
	}
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}


}
