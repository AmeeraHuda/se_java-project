package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import data.CustomerDAO;
import data.CustomerDAOSQLImpl;
import data.FlowerArrangementDAO;
import data.OrderDAO;
import data.OrderDAOSQLImpl;
import objects.Customer;
import objects.FlowerArrangement;
import objects.Order;

/**
 * This program implements the OrderService interface along with keeping track of changes.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/20/2018
 *
 */
public class OrderServiceImpl extends Observable implements OrderService {
	CustomerDAO customers = new CustomerDAOSQLImpl();
	OrderDAO orders = new OrderDAOSQLImpl();

	@Override
	public Response newOrder(Customer customer, List<FlowerArrangement> arrangements, Order order) {
		// TODO Auto-generated method stub
		
		if( (order.getMethodOfRecieving()).equals("") ) {
			return new Response(false, "Pick a Method of Recieveing");
		}
		
		if( customers.insert(customer) == false ) {
			return new Response(false, "Order Not Saved!");
		}
		
		customers.setID(customer);
		
		Double total = 0.0;
		List<Long> arrIDS = new ArrayList<Long>();
		for( FlowerArrangement e: arrangements ) {
			total += e.getPrice();
			arrIDS.add(e.getID());
		}
		
		order.setCustomerID(customer.getCustomerID());
		order.setArrangementIDS(arrIDS);
		order.setOrderTotal(Math.floor(total*100) / 100);
		
		if( orders.insert(order) == false ) {
			return new Response(false, "Order Not Saved!");
		}
		
		setChanged();

		Map<String, Integer> change = new HashMap<>();
		change.put("new", orders.countOfOrders());

		notifyObservers(change);

		return new Response(true, "Order Successfully Saved!");
	}

	@Override
	public Response updateOrder(List<FlowerArrangement> arrangements, Order order) {
		// TODO Auto-generated method stub
		
		if( arrangements != null) {
			double total = 0.0;
			List<Long> arrIDS = new ArrayList<Long>();
			for( FlowerArrangement e: arrangements ) {
				total += e.getPrice();
				arrIDS.add(e.getID());
			}
			order.setArrangementIDS(arrIDS);
			order.setOrderTotal(Math.floor(total*100) / 100);
		}
		
		if( orders.update(order) == false ) {
			return new Response(false, "Order Not Updated!");
		}
		
		setChanged();

		return new Response(true, "Order Successfully Updated!");
	}

	@Override
	public Response deleteOrder(Order order) {
		// TODO Auto-generated method stub
		
		
		if( orders.delete(order) == false ) {
			return new Response(false, "Order Not Deleted!");
		}
		
		setChanged();

		Map<String, Integer> change = new HashMap<>();
		change.put("delete", orders.countOfOrders());

		notifyObservers(change);

		return new Response(true, "Order Successfully Deleted!");
	}

	@Override
	public List<Order> getAllOrders() {
		// TODO Auto-generated method stub
		return orders.getOrders();
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return orders.countOfOrders();
	}

	@Override
	public Response updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		if( customers.update(customer) == false ) {
			return new Response(false, "Customer Not Updated!");
		}
		
		setChanged();

		return new Response(true, "Customer Successfully Updated!");
	}

	@Override
	public Response newCustomer(Customer customer, List<FlowerArrangement> arrangements) {
		// TODO Auto-generated method stub
		if( (customer.getName()).equals("") ) {
			return new Response(false, "Name Field Empty");
		} else if( (customer.getEmail()).equals("") ) {
			return new Response(false, "Email Field Empty");
		} else if( (customer.getAddress()).equals("") ) {
			return new Response(false, "Address Field Empty");
		} else if( (customer.getPhoneNumber()).equals("") ) {
			return new Response(false, "Number Field Empty");
		}
		
		if( arrangements == null ) {
			return new Response(false, "Pick an Arrangement");
		}
		
		setChanged();
		
		return new Response(true, "Ready To Move Forward");
	}

}
