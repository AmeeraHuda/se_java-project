package service;


import java.util.List;

import objects.Customer;
import objects.FlowerArrangement;
import objects.Order;

/**
 * This program has the functionalities needed to communicate 
 * from the user interface to the database, while keeping in track of all the changes.
 * 
 * @author Ameera Huda
 * @version 1.0
 * @since 04/20/2018
 *
 */
public interface OrderService {
	
	/**
	 * Inserts customer information into the table along with order 
	 * information in the order table.
	 * 
	 * @param customer associated with order
	 * @param arrangements associated with order
	 * @param order order to be inserted in table
	 * @return whether or not insertion was successful
	 */
	public Response newOrder(Customer customer, List<FlowerArrangement> arrangements, Order order);
	
	/**
	 * Updates information of the given order.
	 * 
	 * @param arrangements associated with the order update
	 * @param order updated order
	 * @return whether or not update was successful
	 */
	public Response updateOrder(List<FlowerArrangement> arrangements, Order order);
	
	/**
	 * Delete the given order.
	 * 
	 * @param order to be deleted
	 * @return whether or not deletion was successful
	 */
	public Response deleteOrder(Order order);
	
	/**
	 * Produces a list of all the orders.
	 * 
	 * @return list of the orders saved.
	 */
	public List<Order> getAllOrders();
	
	/**
	 * Produces the size of the table.
	 * 
	 * @return number of orders
	 */
	public Integer getSize();
	
	
	/**
	 * Inserts customer information into Customer table.
	 * 
	 * @param customer associated to order
	 * @param arrangements associated to order
	 * @return whether or not customer insertion was successful
	 */
	public Response newCustomer(Customer customer, List<FlowerArrangement> arrangements);
	
	/**
	 * Updates customer information.
	 * @param customer that needs to updated
	 * @return whether or not update was successful
	 */
	public Response updateCustomer(Customer customer);
	
}
